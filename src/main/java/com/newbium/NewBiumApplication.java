package com.newbium;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.newbium.schedular.Main;

@SpringBootApplication
@ComponentScan("com.newbium")
@EnableJpaRepositories(basePackages = {"com.newbium"})
public class NewBiumApplication {
    public static void main(String[] args) {
        SpringApplication.run(NewBiumApplication.class, args);
        Main.twitterSchedular();
    }
}
