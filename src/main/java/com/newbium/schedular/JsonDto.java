package com.newbium.schedular;

import java.io.File;

public class JsonDto {
	private String nxtId;
	private String groupID;
	private String title;
	private String message;
	private File file;
	private String fileName;
	public String getNxtId() {
		return nxtId;
	}
	public void setNxtId(String nxtId) {
		this.nxtId = nxtId;
	}
	public String getGroupID() {
		return groupID;
	}
	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public File getFile() {
		return file;
	}
	public void setFile(File file) {
		this.file = file;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	

}
