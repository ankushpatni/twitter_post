package com.newbium.schedular;

import twitter4j.*;
import twitter4j.auth.OAuth2Token;
import twitter4j.conf.ConfigurationBuilder;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class Main {

	private static final String CONSUMER_KEY = "HKnswFIlKIoDdpwRSUqhqudE2";
	private static final String CONSUMER_SECRET = "DkggFKxTuKQdohMkrvp5JDMG89d5H5cpsIpMlFWJ3y6xuZYm43";
	private static String AccessToken = "955687389673828352-19T0kgMjEH9GJhtuG19mrIhIDCGb2UB";
	private static String AccessSecret = "ddcaegt0ZaeTTUhFOmXDg21iUAF0sVFJPa1mCaZ4nUlnZ";
	private static final int TWEETS_PER_QUERY = 5; // 100
	private static final int MAX_QUERIES = 2; // 5
	private static final String SEARCH_TERM = "@Bitcoin";

	public static String cleanText(String text) {
		text = text.replace("\n", "\\n");
		text = text.replace("\t", "\\t");
		return text;
	}

	public static OAuth2Token getOAuth2Token() {
		OAuth2Token token = null;
		ConfigurationBuilder cb;
		cb = new ConfigurationBuilder();
		cb.setApplicationOnlyAuthEnabled(true);
		cb.setOAuthConsumerKey(CONSUMER_KEY).setOAuthConsumerSecret(CONSUMER_SECRET);
		try {
			token = new TwitterFactory(cb.build()).getInstance().getOAuth2Token();
		} catch (Exception e) {
			System.out.println("Could not get OAuth2 token");
			e.printStackTrace();
			System.exit(0);
		}
		return token;
	}

	public static Twitter getTwitter() {
		OAuth2Token token;
		token = getOAuth2Token();
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setApplicationOnlyAuthEnabled(true);
		cb.setOAuthConsumerKey(CONSUMER_KEY);
		cb.setOAuthConsumerSecret(CONSUMER_SECRET);
		cb.setOAuth2TokenType(token.getTokenType());
		cb.setOAuth2AccessToken(token.getAccessToken());
		return new TwitterFactory(cb.build()).getInstance();
	}

	// public static void main(String[] args){
	@Scheduled(cron = " 0 0/10 * 1/1 * ? ")
	public static void callScheduler() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.MINUTE, -10);
		System.out.println(cal.getTime());
		twitterSchedular();
	}
	
	 public void Main(String key, String data) {
	        FileOutputStream fileOut = null;
	        FileInputStream fileIn = null;
	        try {
	            Properties configProperty = new Properties();
	            File file = new File("src/main/resources/application.properties");
	            fileIn = new FileInputStream(file);
	            configProperty.load(fileIn);
	            configProperty.setProperty(key, data);
	            fileOut = new FileOutputStream(file);
	            configProperty.store(fileOut, "write SinceID properties");
	        } catch (Exception ex) {
	        	ex.printStackTrace();
	        } finally {

	            try {
	                fileOut.close();
	            } catch (IOException ex) {
	            	ex.printStackTrace();
	            }
	        }
	    }
	 
	public static void twitterSchedular() {
		Properties properties = new Properties();
		try {
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		String groupId = properties.getProperty("groupID");
		String nextId = properties.getProperty("nxtID");
		String user = properties.getProperty("user");
		long longSinceID = Long.parseLong(properties.getProperty("sinceId"));
		long tempSinceID = Long.parseLong(properties.getProperty("sinceId"));
		String textUrl=null;
		int totalTweets = 0;
		Twitter twitter = getTwitter();
		List<JsonDto> dtoList = new ArrayList<>();
		String strSinceId =null;
		int count=0;
		List<Status> statuses = new ArrayList<>();
		try {
			 Paging page = new Paging(longSinceID);
			//Paging page = new Paging(pageno++,5,longSinceID);
			statuses.addAll(twitter.getUserTimeline(user, page));		
			for (Status s: statuses) { // Loop through all the tweets..
				System.out.println((s.getId()<=tempSinceID)+"-------------------------------------"+count);
				if(s.getId()<=tempSinceID || s.getRetweetedStatus()!=null){
					continue;
				}
				if(count==0){
					longSinceID=s.getId();
					strSinceId=Long.toString(longSinceID);
					System.out.println("sinceId@@@@@=="+strSinceId);
				}
				System.out.println("getId="+s.getId());
				count++;
				/*if (s.getRetweetedStatus()!=null) {
						continue;
				}*/
				textUrl=null;
				totalTweets++;
				//System.out.println("retweet_status==="+s.getRetweetedStatus()+"==count="+s.getRetweetCount());
				System.out.printf("At %s, @%-20s said:  %s\n", s.getCreatedAt().toString(), s.getUser().getScreenName(),cleanText(s.getText()));
				MediaEntity[] media = s.getMediaEntities();
				if(media.length==0){
					URLEntity[] urlEntity=s.getURLEntities();
					for(URLEntity r:urlEntity){
						textUrl=r.getExpandedURL();
						break;
					}
				}
				File file = new File("src/main/resources/images");
				JsonDto vm = new JsonDto();
				for (MediaEntity m : media) {
					try {
						System.out.println("url==" + m.getMediaURL());
						URL url = new URL(m.getMediaURL());
						InputStream in = new BufferedInputStream(url.openStream());
						ByteArrayOutputStream out = new ByteArrayOutputStream();
						byte[] buf = new byte[1024];
						int n = 0;
						while (-1 != (n = in.read(buf))) {
							out.write(buf, 0, n);
						}
						out.close();
						in.close();
						byte[] response = out.toByteArray();
						FileOutputStream fos = new FileOutputStream(
								file.getAbsolutePath() + "/" + m.getId() + "." + getExtension(m.getType()));
						fos.write(response);
						vm.setFile(
								new File(file.getAbsolutePath() + "/" + m.getId() + "." + getExtension(m.getType())));
						vm.setFileName(Long.toString(m.getId()));
						fos.close();
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
				String str=s.getText();
				if(str!=null && !str.isEmpty()){
					str = str.substring(0, Math.min(str.length(), 100));
				}
				//String title = s.getInReplyToScreenName() != null ? s.getInReplyToScreenName() : "Bitcoin";
				vm.setGroupID(groupId);
				if(textUrl!=null){
					vm.setMessage(s.getText()+" \n <a href="+"\'"+textUrl+"\'"+">"+textUrl+"</a>");
				}else{
					vm.setMessage(s.getText());
				}
				vm.setNxtId(nextId);
				vm.setTitle(str);
				dtoList.add(vm);
			}
			Main help = new Main();
			if(strSinceId!=null){
				help.Main("sinceId", strSinceId);
			}else{
				help.Main("sinceId", properties.getProperty("sinceId"));
			}
			System.out.println("string sinceId"+longSinceID);
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		System.out.printf("\n\nA total of %d tweets retrieved\n", totalTweets);
		for (JsonDto list : dtoList) {
			System.out.println("title=="+list.getTitle());
			System.out.println("message==="+list.getMessage());
			CloseableHttpClient httpClient = HttpClients.createDefault();
			HttpPost uploadFile = new HttpPost("https://feed.newbium.com/nxt/groups/create_post");
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.addTextBody("nxtID", list.getNxtId(), ContentType.TEXT_PLAIN);
			builder.addTextBody("groupID", list.getGroupID(), ContentType.TEXT_PLAIN);
			builder.addTextBody("title", list.getTitle(), ContentType.TEXT_PLAIN);
			builder.addTextBody("message", list.getMessage(), ContentType.TEXT_PLAIN);
			if (list.getFile() != null) {
				try {
					builder.addBinaryBody("file", new FileInputStream(list.getFile()), ContentType.APPLICATION_OCTET_STREAM, list.getFile().getName());
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			} else {
				byte[] b = "".getBytes();
				builder.addBinaryBody("file", b);
			}
			HttpEntity multipart = builder.build();
			uploadFile.setEntity(multipart);
			CloseableHttpResponse response = null;
			try {
				response = httpClient.execute(uploadFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
			HttpEntity responseEntity = response.getEntity();
			System.out.println("response===" + response.toString());
		}
	}

	public static String getExtension(String type) {
		if (type.equals("photo")) {
			return "jpg";
		} else if (type.equals("video")) {
			return "mp4";
		} else if (type.equals("animated_gif")) {
			return "gif";
		} else {
			return "err";
		}
	}
}